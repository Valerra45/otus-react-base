import React, { useState } from 'react';
import axios from "axios";

function Login() {
    const API_URL = "http://localhost:5000/api/auth/";

    const [username, setUserName] = useState('');
    const [password, setPassword] = useState('');

    function loginSubmit(e: React.FormEvent) {
        e.preventDefault()

        axios.post(API_URL + "signin", {
            username,
            password
        })
            .then(response => {
                console.log('ok');
            })
            .catch(err => {
                console.log('err');
            });
    }

    return (
        <form onSubmit={loginSubmit}>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Username" onChange={e => setUserName(e.target.value)} required />
            </div>
            <div>
                <label>Password: </label>
                <input type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} required />
            </div>
            <button type="submit" className="button">
                Login
            </button>
        </form>
    );
}

export default Login;